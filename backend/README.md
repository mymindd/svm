# Locally run Backebnd

## Requirements

- [Poetry](https://python-poetry.org/) for Python package and environment management.
- [MongoDB](https://www.mongodb.com/docs/manual/installation/) Database for storage product in stocks.

First, install packages:

```bash
poetry install
```

- If don't install poetry you can use command: pip install poetry

After that, set Environment:

```bash
cp .env.example .env
```

- You can change MONGODB variable setting in file .env

Finally, run project with:

```bash
uvicorn app.main:app --reload
```

## Docs

You can see all path of api via: http://localhost:8000/docs
