import io

from fastapi import APIRouter, Form, HTTPException, Response, UploadFile
from starlette.responses import StreamingResponse
from starlette.status import (HTTP_201_CREATED, HTTP_400_BAD_REQUEST,
                              HTTP_404_NOT_FOUND)

from app import models
from app.schemas import ListOfProduct, ProductInCreate, ProductInResponse

router = APIRouter()


@router.get("/", response_model=ListOfProduct)
def get_products(pages: int = 1):
    items_per_page = 10
    offset = (pages - 1) * items_per_page
    total_pages = models.Product.objects.count() / items_per_page
    products = (
        models.Product.objects.order_by("name").skip(offset).limit(items_per_page)
    )
    return ListOfProduct(
        products=list(products),
        total_pages=total_pages
        if total_pages % 1 == 0 and total_pages != 0
        else total_pages + 1,
        current_pages=pages,
        items_per_page=items_per_page,
    )


@router.get("/available", response_model=ListOfProduct)
def get_available_products():
    products = models.Product.objects(quantity__gt=0)
    return ListOfProduct(products=list(products))


@router.get("/{product_id}", response_model=ProductInResponse)
def get_product(product_id: str):
    try:
        product = models.Product.objects.get(id=product_id)
        data = product.to_mongo()
        if product.image:
            data["image_url"] = f"products/{str(product.id)}/image"
        return data
    except Exception:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Not found this product",
        )


@router.get("/{product_id}/checkout", response_class=Response)
def checkout_product(product_id: str):
    try:
        product = models.Product.objects.get(id=product_id)
        if product.quantity == 0:
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Product not available",
            )
        product.update(quantity=product.quantity - 1)

    except Exception:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Not found this product",
        )


@router.post(
    "/",
    response_model=ProductInResponse,
    name="products:create",
    status_code=HTTP_201_CREATED,
)
def create_product(product_create: ProductInCreate):
    if not product_create.name:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail="Please enter Product Name",
        )

    product = models.Product.objects(name=product_create.name).first()
    if product:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail="Product Name already exists.",
        )
    product = models.Product(
        name=product_create.name,
        prices=product_create.prices,
        quantity=product_create.quantity,
    ).save()
    return product.to_mongo()


@router.post("/{product_id}/upload_image", response_class=Response)
def upload_product_image(product_id: str, image: UploadFile = Form(...)):
    try:
        product = models.Product.objects.get(id=product_id)
    except Exception:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Not found this product",
        )
    if product.image:
        product.image.replace(
            image.file, filename=image.filename, content_type=image.content_type
        )
    else:
        product.image.put(
            image.file, filename=image.filename, content_type=image.content_type
        )
    product.save()


@router.get("/{product_id}/image", response_class=Response)
def get_product_image(product_id: str, type: str = "image"):
    try:
        product = models.Product.objects.get(id=product_id)
    except Exception:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Not found this product",
        )
    content = (
        io.BytesIO(product.image.read())
        if type == "image"
        else io.BytesIO(product.image.thumbnail.read())
    )
    return StreamingResponse(content, media_type=product.image.content_type)


@router.put("/{product_id}", response_model=ProductInResponse)
def update_product(product_id: str, product_update: ProductInCreate):
    product_check = models.Product.objects(
        id__ne=product_id, name=product_update.name
    ).first()
    if product_check:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail="Product Name already exists.",
        )
    try:
        product = models.Product.objects.get(id=product_id)
        product.update(
            name=product_update.name,
            prices=product_update.prices,
            quantity=product_update.quantity,
        )
        return product.to_mongo()

    except Exception:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Not found this product",
        )


@router.delete("/{product_id}", response_class=Response)
def delete_product(product_id: str):
    try:
        product = models.Product.objects.get(id=product_id)
        if product.image:
            product.image.delete()
        product.delete()
    except Exception:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Not found this product",
        )
