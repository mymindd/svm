from functools import lru_cache
from typing import Any, Dict, List

from pydantic import BaseSettings, SecretStr


class AppSettings(BaseSettings):
    debug: bool = False
    docs_url: str = "/docs"
    redoc_url: str = "/redoc"
    title: str = "FastAPI for  Simple Vending Machine application"
    version: str = "0.1.0"

    secret_key: SecretStr

    api_prefix: str = "/api"

    allowed_hosts: List[str] = ["*"]

    mongodb_db: str = "svmdb"
    mongodb_host: str = "localhost"
    mongodb_port: int = 27017
    mongodb_username: str = ""
    mongodb_password: str = ""

    class Config:
        env_file = ".env"

    @property
    def fastapi_kwargs(self) -> Dict[str, Any]:
        return {
            "debug": self.debug,
            "docs_url": self.docs_url,
            "redoc_url": self.redoc_url,
            "title": self.title,
            "version": self.version,
        }


@lru_cache
def get_app_settings() -> AppSettings:
    return AppSettings()
