import mongoengine as me

from .products import Product


def init_mongoengine(settings):

    dbname = settings.mongodb_db
    host = settings.mongodb_host
    port = settings.mongodb_port
    username = settings.mongodb_username
    password = settings.mongodb_password

    me.connect(db=dbname, host=host, port=port, username=username, password=password)
