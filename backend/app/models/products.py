import datetime

from mongoengine import (DateTimeField, Document, FloatField, ImageField,
                         IntField, StringField)


class Product(Document):
    meta = {"collection": "products"}

    name = StringField(required=True)
    prices = FloatField(required=True)
    quantity = IntField(min_value=0, required=True, default=0)

    image = ImageField(thumbnail_size=(300, 300), collection_name="products_images")

    created_date = DateTimeField(required=True, default=datetime.datetime.now)
    updated_date = DateTimeField(required=True, default=datetime.datetime.now)

    @property
    def image_url(self):
        if self.image:
            return f"products/{str(self.id)}/image"
        return ""
