from typing import List

from pydantic import BaseModel

from .base import BaseSchema


class ProductInCreate(BaseModel):
    name: str
    prices: float
    quantity: float = 0


class ProductInResponse(BaseSchema):
    name: str
    prices: float
    quantity: float = 0
    image_url: str = ""


class ListOfProduct(BaseSchema):
    products: List[ProductInResponse] = []
    total_pages: int = 1
    current_pages: int = 1
    items_per_page: int = 0
