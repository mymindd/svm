# Simple Vending Machine(SVM)

## Requirements

- [Docker](https://www.docker.com/).
- [Docker Compose](https://docs.docker.com/compose/install/).

First, set Environment:

```bash
cp ./backend/.env.example ./backend/.env
```

After that, run project with:

```bash
docker-compose up
```

## Application Pages

Path for frontend

| Path                         | Description                        |
| ---------------------------- | ---------------------------------- |
| http://localhost:3000/       | First page of web application      |
| http://localhost:3000/stocks | Page for manage products in stocks |

## Docs for API

You can see all path of api via: http://localhost:8000/docs

## Description

ในการออกแบบ ในหน้าแรก http://localhost:3000/ ได้มีการออกแบบให้แสดงเฉพาะสินค้าที่มีอยู่ใม stock คือ product.quantity > 0
ดังนั้น ในการเข้ามาในครั้งแรก ควรเข้าไปที่ http://localhost:3000/stocks เพื่อ เพิ่มสินค้า เข้าไปใน stock และอย่าลืมปรับปรุงค่า quantity ให้มีค่า มากกว่า 0 ด้วย
