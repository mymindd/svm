## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

## Pages

Path for frontend

| Path                         | Description                        |
| ---------------------------- | ---------------------------------- |
| http://localhost:3000/       | First page of web application      |
| http://localhost:3000/stocks | Page for manage products in stocks |
