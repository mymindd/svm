import React from "react";

function ImageUpload({ onChangeCB }) {
  return (
    <div className="grid rounded-lg">
      <label className="inline-block mb-2 text-gray-500">
        Upload Product Image(jpg,png,svg,jpeg)
      </label>

      <div className="flex items-center justify-center w-full">
        <label className="flex flex-col w-full h-32 border-4 border-dashed hover:bg-gray-100 hover:border-gray-300">
          <div className="flex flex-col items-center justify-center pt-7">
            <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
              Select a image
            </p>
          </div>
          <input
            onChange={onChangeCB}
            type="file"
            accept="image/*"
            className="opacity-0"
          />
        </label>
      </div>
    </div>
  );
}

export default ImageUpload;
