function TextInput({ label, name, value, onChangeCB }) {
  return (
    <div className="block">
      <span className="text-gray-700">{label}</span>
      <input
        className="
                    mt-1
                    block
                    w-full
                    rounded-md
                    border-gray-300
                    shadow-sm
                    focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50
                  "
        type="text"
        name={name}
        value={value}
        onChange={onChangeCB}
      />
    </div>
  );
}

export default TextInput;
