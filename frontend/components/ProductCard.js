import Button from "../components/Button";
import Router from "next/router";

function ProductCard({ id, name, prices, quantity, image_url }) {
  const handleBuy = async (e) => {
    Router.push(`/checkout/${id}`);
  };

  return (
    <div className="bg-white rounded-md my-1 sm:my-3 sm:border">
      <div className="flex justify-center p-3">
        <div className="font-semibold capitalize text-lg">{name}</div>
      </div>
      <div className="flex w-full justify-center">
        {image_url ? (
          <img
            className="rounded-sm object-cover w-96 h-96 "
            src={image_url}
            alt=""
          />
        ) : (
          <div className="text-center py-36 rounded-sm  w-96 h-96 bg-gray-400 ">
            <span className="text-white">No Image</span>
          </div>
        )}
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-6 m-4">
        <div className="text-lg text-gray-700 text-center">
          <span className="text-3xl mt-2 text-green-800 mr-2">
            {prices.toLocaleString(undefined, {
              maximumFractionDigits: 2,
            })}
          </span>
          Baht
          <div className="text-gray-700 text-sm">
            <span className="font-semibold">Quantity: </span>
            {quantity.toLocaleString(undefined, {
              maximumFractionDigits: 2,
            })}
          </div>
        </div>
        <div className="w-full">
          <Button
            id={id}
            text="Buy"
            colorBtn="blue"
            onClickHandle={handleBuy}
          />
        </div>
      </div>
    </div>
  );
}

export default ProductCard;
