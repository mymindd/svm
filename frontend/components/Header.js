import { APP_NAME, APP_NAME_SHORT } from "../libs/utils/constant";

function Header() {
  return (
    <div className="flex justify-center h-16 bg-blue-700 border-b">
      <div className="flex text-center items-center mt-2">
        <h1 className="hidden sm:inline-grid font-semibold text-2xl text-white">
          <a href="/">{APP_NAME}</a>
        </h1>
        <h1 className="sm:hidden font-semibold text-2xl text-white">
          <a href="/">{APP_NAME_SHORT}</a>
        </h1>
      </div>
    </div>
  );
}

export default Header;
