import { useEffect, useState } from "react";
import ProductCard from "./ProductCard";
import ProductAPI from "../libs/api/products";
import { SERVER_BASE_URL } from "../libs/utils/constant";

function Products() {
  const [products, setProducts] = useState([]);
  const [isProducts, setIsProducts] = useState(false);

  useEffect(() => {
    async function fetchMyAPI() {
      const { data, status } = await ProductAPI.getAvailableProducts();
      if (status !== 200) {
        return;
      }
      console.log(products);
      setProducts(data.products);
      console.log(products.length);
      if (data.products.length > 0) {
        setIsProducts(true);
      }
    }
    fetchMyAPI();
  }, []);

  return (
    <div className="py-8">
      <div
        hidden={isProducts}
        className="bg-white mt-20 max-w-lg mx-auto py-6 divide-y md:max-w-4xl rounded-md"
      >
        <div className="m-4 text-center font-bold text-gray-700 text-5xl w-full">
          No any available products, Please contact support.
        </div>
      </div>

      <div
        className="grid grid-cols-1 md:grid-cols-2 md:max-w-2xl
    xl:grid-cols-3 xl:max-w-7xl mx-auto col-span-2 gap-10 "
      >
        {products.map((product) => (
          <ProductCard
            key={product._id}
            id={product._id}
            name={product.name}
            prices={product.prices}
            quantity={product.quantity}
            image_url={
              product.image_url ? `${SERVER_BASE_URL}/${product.image_url}` : ""
            }
          />
        ))}
      </div>
    </div>
  );
}

export default Products;
