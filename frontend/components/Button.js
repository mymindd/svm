import React from "react";

function Button({ btnId, onClickHandle, text, colorBtn }) {
  return (
    <button
      id={btnId}
      onClick={onClickHandle}
      className={`m-2 w-full rounded-md bg-blue bg-${colorBtn}-500 hover:bg-${colorBtn}-700 h-10 text-white`}
    >
      {text}
    </button>
  );
}

export default Button;
