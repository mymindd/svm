function Pagination({ maxPages, currentPages, setCurrentPages, setIsUpdate }) {
  const handleFirstPages = (e) => {
    setCurrentPages(1);
    setIsUpdate(true);
  };
  const handleSelectPages = (e) => {
    console.log(e.target.text);
    setCurrentPages(parseInt(e.target.text));
    setIsUpdate(true);
  };
  const handleLastPages = (e) => {
    setCurrentPages(maxPages);
    setIsUpdate(true);
  };

  return (
    <div className="flex justify-center">
      <ul className="pagination bg-white p-2 shadow-sm rounded">
        <li className="inline-block p-2">
          {currentPages === 1 ? (
            <span className="rounded-l rounded-sm border bg-gray-100 border-gray-100 px-3 py-2 cursor-not-allowed no-underline text-gray-600 h-10">
              &laquo;
            </span>
          ) : (
            <a
              onClick={handleFirstPages}
              className="rounded-r rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 text-gray-600 no-underline"
            >
              &laquo;
            </a>
          )}
        </li>
        {[...Array(maxPages)].map((page, idx) => (
          <li key={idx} className="inline-block">
            {currentPages === idx + 1 ? (
              <span className="rounded-l rounded-sm border bg-gray-100 border-gray-100 px-3 py-2 cursor-not-allowed no-underline text-gray-600 h-10">
                {idx + 1}
              </span>
            ) : (
              <a
                onClick={handleSelectPages}
                className="rounded-r rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 text-gray-600 no-underline"
              >
                {idx + 1}
              </a>
            )}
          </li>
        ))}
        <li className="inline-block p-2">
          {currentPages === maxPages ? (
            <span className="rounded-l rounded-sm border bg-gray-100 border-gray-100 px-3 py-2 cursor-not-allowed no-underline text-gray-600 h-10">
              &raquo;
            </span>
          ) : (
            <a
              onClick={handleLastPages}
              className="rounded-r rounded-sm border border-gray-100 px-3 py-2 hover:bg-gray-100 text-gray-600 no-underline"
            >
              &raquo;
            </a>
          )}
        </li>
      </ul>
    </div>
  );
}

export default Pagination;
