import axios from "axios";

import { SERVER_BASE_URL } from "../utils/constant";

const ProductAPI = {
  create: async (productName, productPrice, productQuantity) => {
    try {
      const response = await axios.post(
        `${SERVER_BASE_URL}/products`,
        // formData,
        JSON.stringify({
          name: productName,
          prices: productPrice,
          quantity: productQuantity,
        }),
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      return response;
    } catch (error) {
      return error.response;
    }
  },
  update: async (productId, productName, productPrice, productQuantity) => {
    try {
      const response = await axios.put(
        `${SERVER_BASE_URL}/products/${productId}`,
        JSON.stringify({
          name: productName,
          prices: productPrice,
          quantity: productQuantity,
        }),
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      return response;
    } catch (error) {
      return error.response;
    }
  },
  upload_image: async (productId, productImage) => {
    let formData = new FormData();
    formData.append("image", productImage);

    try {
      const response_image = await axios.post(
        `${SERVER_BASE_URL}/products/${productId}/upload_image`,
        formData
      );
    } catch (error) {
      return error.response;
    }
  },
  getAllProducts: async (pages) => {
    try {
      const response = await axios.get(
        `${SERVER_BASE_URL}/products?pages=${pages}`
      );
      return response;
    } catch (error) {
      return error.response;
    }
  },
  getAvailableProducts: async (pages) => {
    try {
      const response = await axios.get(`${SERVER_BASE_URL}/products/available`);
      return response;
    } catch (error) {
      return error.response;
    }
  },
  getProduct: async (productId) => {
    try {
      const response = await axios.get(
        `${SERVER_BASE_URL}/products/${productId}`
      );
      return response;
    } catch (error) {
      return error.response;
    }
  },
  checkout: async (productId) => {
    try {
      const response = await axios.get(
        `${SERVER_BASE_URL}/products/${productId}/checkout`
      );
      return response;
    } catch (error) {
      return error.response;
    }
  },
  delete: async (productId) => {
    try {
      console.log(productId);
      const response = await axios.delete(
        `${SERVER_BASE_URL}/products/${productId}`
      );
      return response;
    } catch (error) {
      return error.response;
    }
  },
};

export default ProductAPI;
