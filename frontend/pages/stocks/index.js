import Head from "next/head";
import { useEffect, useState } from "react";
import Header from "../../components/Header";
import { APP_NAME, SERVER_BASE_URL } from "../../libs/utils/constant";
import ProductAPI from "../../libs/api/products";
import Pagination from "../../components/Pagination";
import Button from "../../components/Button";
import Router from "next/router";

export default function StockList() {
  const [products, setProducts] = useState([]);
  const [isProducts, setIsProducts] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [maxPages, setMaxPages] = useState(1);
  const [currentPages, setCurrentPages] = useState(1);

  useEffect(() => {
    async function fetchMyAPI() {
      const { data, status } = await ProductAPI.getAllProducts(currentPages);
      if (status !== 200) {
        return;
      }
      setProducts(data.products);
      setIsUpdate(false);
      console.log(products.length);
      if (data.products.length > 0) {
        setIsProducts(true);
      }
      setMaxPages(data.total_pages);
    }
    fetchMyAPI();
  }, [isUpdate]);

  const handleDelete = async (e) => {
    let productId = e.target.id;
    e.preventDefault();
    try {
      const { data, status } = await ProductAPI.delete(productId);
      setIsUpdate(true);
    } catch (error) {
      console.error(error);
    }
  };

  const handleUpdate = async (e) => {
    Router.push(`/stocks/update/${e.target.id}`);
  };

  return (
    <div
      className="bg-gray-100 h-screen 
    overflow-y-scroll"
    >
      <Head>
        <title>Stocks | {APP_NAME}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* Header */}

      <Header />

      <div className="container mx-auto px-4 sm:px-8">
        <div className="py-8">
          <div>
            <h2 className="text-2xl font-semibold leading-tight text-gray-700">
              Stocks
            </h2>
            <div className="flex justify-end">
              <a href="/stocks/add">
                <span className="border border-blue-500 p-3 ml-5 rounded-md text-sm text-blue-500">
                  Add New Product
                </span>
              </a>
            </div>
          </div>
          <div className="-mx-4 sm:-mx-8 px-4 sm:px-6 py-4 overflow-x-auto">
            <div className="inline-block min-w-full shadow-md rounded-lg overflow-hidden">
              <table className="min-w-full leading-normal">
                <thead>
                  <tr>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-50 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                      Name
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-50 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                      Price
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-50 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                      Quantity
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-50 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {products.map((product) => (
                    <tr key={product._id}>
                      <td className="px-5 py-10 border-b border-gray-200 bg-white text-sm">
                        <div className="flex">
                          <div className="flex-shrink-0 w-20 h-20">
                            {product.image_url ? (
                              <img
                                className="w-full h-full rounded-full"
                                src={`${SERVER_BASE_URL}/${product.image_url}?type=thumbnail`}
                                alt=""
                              />
                            ) : (
                              <div className="w-full h-full rounded-full bg-gray-600"></div>
                            )}
                          </div>
                          <div className="ml-3">
                            <p className="text-lg text-gray-900 whitespace-no-wrap font-semibold capitalize mt-6">
                              {product.name}
                            </p>
                          </div>
                        </div>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap">
                          {product.prices.toLocaleString(undefined, {
                            maximumFractionDigits: 2,
                          })}
                        </p>
                        <p className="text-gray-600 whitespace-no-wrap">Baht</p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap">
                          {product.quantity}
                        </p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <div className="flex justify-start">
                          <Button
                            btnId={product._id}
                            onClickHandle={handleUpdate}
                            text="Update"
                            colorBtn="blue"
                          />
                          <Button
                            btnId={product._id}
                            onClickHandle={handleDelete}
                            text="Delete"
                            colorBtn="red"
                          />
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <div hidden={isProducts} className="pl-5 mx-auto my-3">
                <div
                  className="text-sm text-gray-700 bg-gray-100 rounded-lg dark:bg-gray-700 dark:text-gray-300"
                  role="alert"
                >
                  Don't have any product.{" "}
                  <a href="/stocks/add">
                    <span className="font-medium">Add New</span>
                  </a>
                </div>
              </div>
            </div>
            <Pagination
              maxPages={maxPages}
              currentPages={currentPages}
              setCurrentPages={setCurrentPages}
              setIsUpdate={setIsUpdate}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
