import Head from "next/head";
import Header from "../../../components/Header";
import TextInput from "../../../components/forms/TextInput";
import NumberInput from "../../../components/forms/NumberInput";
import ImageUpload from "../../../components/forms/ImageUpload";

import { useState, useCallback, useEffect } from "react";
import ProductAPI from "../../../libs/api/products";
import Router, { useRouter } from "next/router";
import { APP_NAME, SERVER_BASE_URL } from "../../../libs/utils/constant";

export default function updateStock() {
  //     return productId;
  //   };
  const router = useRouter();

  const [errors, setErrors] = useState("");
  const [productId, setProductId] = useState("");
  const [productName, setProductName] = useState("");
  const [productPrice, setProductPrice] = useState(0.0);
  const [productQuantity, setProductQuantity] = useState(0);
  const [imagePreview, setImagePreview] = useState("");
  const [productImage, setProductImage] = useState("");

  useEffect(() => {
    async function fetchMyAPI() {
      if (!router.isReady) return;
      const { productId } = router.query;
      setProductId(productId);
      const { data, status } = await ProductAPI.getProduct(productId);
      if (status !== 200) {
        return;
      }
      setProductName(data.name);
      setProductPrice(data.prices);
      setProductQuantity(data.quantity);
      if (data.image_url) {
        setImagePreview(`${SERVER_BASE_URL}/${data.image_url}?type=thumbnail`);
      }
    }
    fetchMyAPI();
  }, [router.isReady]);

  const handleProductNameChange = useCallback(
    (e) => setProductName(e.target.value),
    []
  );
  const handleProductPriceChange = useCallback(
    (e) => setProductPrice(parseFloat(e.target.value)),
    []
  );
  const handleProductQuantityChange = useCallback(
    (e) => setProductQuantity(parseInt(e.target.value)),
    []
  );
  const handleProductImageChange = useCallback((e) => {
    if (e.target.files[0]) {
      setImagePreview(URL.createObjectURL(e.target.files[0]));
    }
    setProductImage(e.target.files[0]);
  }, []);
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const { data, status } = await ProductAPI.update(
        productId,
        productName,
        productPrice,
        productQuantity
      );
      if (status !== 200) {
        setErrors(data.errors);
        return;
      }

      if (data) {
        console.log(data);
        let productId = data._id;
        if (productImage) {
          const response = await ProductAPI.upload_image(
            productId,
            productImage
          );
        }
        Router.push("/stocks");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="bg-gray-100 h-screen scorllbar-hide">
      <Head>
        <title>Update | {APP_NAME}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />

      <div className="bg-white mt-20 max-w-lg mx-auto py-6 divide-y md:max-w-4xl rounded-md">
        <div className="mt-2 mb-4 flex justify-center">
          <h2 className="text-lg font-semibold text-gray-700 ">
            Update Product
          </h2>
        </div>
        <form onSubmit={handleSubmit} method="POST">
          <div className="grid grid-cols-1 gap-6 m-4">
            <TextInput
              label="Product Name"
              name="name"
              value={productName}
              onChangeCB={handleProductNameChange}
            />
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-6">
              <NumberInput
                label="Product Price"
                name="price"
                minValue="0.00"
                step="0.1"
                value={productPrice}
                onChangeCB={handleProductPriceChange}
              />
              <NumberInput
                label="Product Quantity"
                name="quantity"
                minValue="0"
                value={productQuantity}
                onChangeCB={handleProductQuantityChange}
              />
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-6">
              {imagePreview ? (
                <img
                  className="rounded-md h-40 w-40 object-cover"
                  src={imagePreview}
                  alt=""
                />
              ) : (
                <div className="rounded-md h-40 w-40 bg-gray-500"></div>
              )}

              <ImageUpload onChangeCB={handleProductImageChange} />
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-6 gap-3">
              <button
                className="rounded-md bg-blue bg-blue-500 hover:bg-blue-700 m-4 h-10 text-white"
                type="submit"
              >
                Submit
              </button>
              <a
                href="/stocks"
                className="text-center pt-2 rounded-md bg-blue bg-gray-500 hover:bg-gray-700 m-4 h-10 text-white"
              >
                Back
              </a>
            </div>
          </div>
        </form>
        {errors && (
          <div
            className="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800"
            role="alert"
          >
            <span className="font-medium">{errors}</span>
          </div>
        )}
      </div>
    </div>
  );
}
