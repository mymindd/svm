import { useState, useEffect } from "react";
import Head from "next/head";
import Header from "../../components/Header";
import Button from "../../components/Button";

import Router, { useRouter } from "next/router";
import ProductAPI from "../../libs/api/products";
import { APP_NAME, SERVER_BASE_URL } from "../../libs/utils/constant";

function checkoutProduct() {
  const router = useRouter();

  const [productId, setProductId] = useState("");
  const [productName, setProductName] = useState("");
  const [productPrice, setProductPrice] = useState(0.0);
  const [imagePreview, setImagePreview] = useState("");

  useEffect(() => {
    async function fetchMyAPI() {
      if (!router.isReady) return;
      const { productId } = router.query;
      setProductId(productId);
      const { data, status } = await ProductAPI.getProduct(productId);
      if (status !== 200) {
        return;
      }
      setProductName(data.name);
      setProductPrice(data.prices);
      if (data.image_url) {
        setImagePreview(`${SERVER_BASE_URL}/${data.image_url}?type=thumbnail`);
      }
    }
    fetchMyAPI();
  }, [router.isReady]);

  const handleConfirm = async (e) => {
    e.preventDefault();
    try {
      const { data, status } = await ProductAPI.checkout(productId);
      if (status !== 200) {
        setErrors(data.errors);
        return;
      }
      Router.push("/");
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div className="bg-gray-100 h-screen scorllbar-hide">
      <Head>
        <title>Update | {APP_NAME}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />

      <div className="bg-white mt-20 max-w-lg mx-auto py-6 divide-y md:max-w-4xl rounded-md">
        <div className="m-4 text-center font-bold text-gray-700 text-5xl">
          Please insert banknotes or coins.
        </div>
        <div className="m-4 text-center font-bold text-gray-700 text-2xl">
          Already Inserted: <span className="text-4xl text-green-900">0 </span>/
          {productPrice.toLocaleString(undefined, {
            maximumFractionDigits: 2,
          })}{" "}
          Baht
        </div>

        <div className="pr-4 grid grid-cols-1 md:grid-cols-2 gap-2 ">
          <Button
            id="confirmBtn"
            text="Confirm (For test insert complete)"
            colorBtn="blue"
            onClickHandle={handleConfirm}
          />
          <Button
            id="backBtn"
            text="Back"
            colorBtn="gray"
            onClickHandle={(e) => {
              Router.push("/");
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default checkoutProduct;
