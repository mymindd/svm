import Head from "next/head";
import Header from "../components/Header";
import Products from "../components/Products";

export default function Home() {
  return (
    <div className="bg-gray-100 h-screen overflow-y-scroll">
      <Head>
        <title>Simple Vending Machine</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {/* Header */}
      <Header />
      {/* Main */}
      <Products />
      {/* Fixed footer */}
    </div>
  );
}
